<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'rci_global');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', '127.0.0.1');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'd/siP9+=;b5fYu,FH.zbrXIKfsK Ko3YaSeNdH7.u-K0X(QAh5($CPM9y.#S}Z4.');
define('SECURE_AUTH_KEY',  's@>)5C:38q/4,*8_D%Q%L^a*Gusl7LRe(U%BU!fr-a)OW@@oz2l&|cIcq&>mtR{Y');
define('LOGGED_IN_KEY',    '?Jy&-2cF#N,JQH)OiIhw~B_@Ipz)~)(o~S*y0%G4_rpu>v+8UBiF%sA;Hf`/O%,n');
define('NONCE_KEY',        'WZv~b;Z3r]2U}om/bZ om>s3r}L~f[t=APW|z<W};Y^J7iAZ?x;<lfHcyX-eXh1d');
define('AUTH_SALT',        'n9]y3FAr`_Sh*8TFG=5#Z!A$jhShsxU6~bgzJ-[(hO$+~*^/aKt~Ri3{Skqi}]Xw');
define('SECURE_AUTH_SALT', '$d?:XwZCQEfm/I=T5JhCn^ui]9bdfk_>da3Pjn;hk~W[[kftUHb[FE16nhzoS;4_');
define('LOGGED_IN_SALT',   'y6^Jf3*ulP`%pUhhPoC(03&^fLdcW53|+@d[uIteM)`oC+BNcfu VM^;yDsl!e}k');
define('NONCE_SALT',       ';|F%W|i5Y(Tp)0>l6+yt4A~$RRJAj8Rqm]mb^~,roysxujm4O1}^KGWd>(Y|oy7~');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');
/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');