<!-- Template Name: RCI Page -->
<div class="row row-head " id="row-head">
	<div class="col-xs-6 col-lg-6 col-logo text-left">
		<?php 
		$image = get_field('rci_global_logo');
		if( !empty($image) ): ?>
			<div class="div_rci_logo">
				<img class="rci_global_logo " src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
				<p class="text-center logo_title"><?php the_field('title_logo'); ?></p> 
			</div>
		<?php endif; ?>
	</div>
	<div class="col-xs-6 col-lg-6 col-newscenter text-right news-menu-align">
		<div class="newcenter-wrap pull-right" id="img_center" onclick="newCenter()">
			<?php 
			$image_newcenter = get_field('new_center_logo');
			if( !empty($image_newcenter) ): ?>
				<span class="title_newscenter pull-left">NEWS<br>CENTER</span>
				<div class="image_newcenter_block pull-left">
					<img class="image_newcenter"  src="http://localhost:8089/RCI%20Global/wp-content/uploads/2018/12/u934.png" alt="<?php echo $image_newcenter['alt']; ?>" />
				</div>
			<?php endif; ?>
		</div>
	</div>
</div>
<!-- End of row-head -->
<!-- Menu -->
<div class="row rcigs-page row-menu" > 
	<div class="col-xs-12 col-xs-offset-0 col-sm-offset-8 col-sm-3 col-md-offset-8 col-md-3 col-lg-offset-10 col-lg-2 col-menu"> 
		<div class="row pull-right news-menu-align" style="margin: 0;">
			<div class="search-box"> 
				<input  id="search-box" type="text"/>
			</div>
		</div>
		<div class="row pull-right" style="margin: 25px 0 0 0;">
		<div class="main-menu" onclick="show_menu_panel()" >
			<div class="menu-icon-title">
				<div class="menu-icon"  onclick="show_menu_panel()">
					<div class="img_menu first"></div>	
					<div class="img_menu"></div>
					<div class="img_menu"></div>
				</div>
				<span id="title-menu"> Menu </span>
			</div>	
			<!-- menu -->
				<div id="menu-background">
					<div id="menu-item-display">
						<?php get_header(); ?>
						<!--site main menu --> 
						<header class="site-header">
								<!-- <div class="container"> -->
								<div class="col-lg-4"> 
									<nav class="navbar navbar site-nav" style="margin-bottom:5px;">
										<!-- <h1><a href="<?php echo home_url(); ?>"><?php bloginfo('name') ?></a></h1>
										<h5><?php bloginfo('description') ?></h5> -->
								<?php $args = array('theme_location' => 'primary') ?>
								<div class="main-menu-item" style="margin-right: 75%;">
								<ul>
									<a href="#"><li class="next" onclick="onClickItem('.page0')">RCI GLOBAL SERVICES</li></a>
									<li class="next" onclick="onClickItem('.page2')">RCI CONSULTING</li>
									<li class="next" onclick="onClickItem('.page3')">RSI SOLUTION</li>
									<a href="#learnmore_cus"><li class="next" onclick="onClickItem('.page4')">CUSTOMERS</li></a>
									<li class="next" onclick="onClickItem('.page5')">RESOURCES</li>
									<li class="next" onclick="onClickItem('.page6')">CONTACT</li>
								</ul>
								</div>
									</nav>
								</div>
						</header>
					</div>
				</div>
			</div>
		</div>
		</div>
</div>
</div>
<!-- End of menu -->