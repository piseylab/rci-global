<div class="heading">
	<?php include_once 'fixed-header.php'; ?>
</div>
<div class="blackground">
<div class="rcigs-page page0 slideInDown" id="main_page">
	<div class="home img_backheader">
        <!-- Row RCI Global Services -->
		<div class="row main-description">
			<div class="col-sm-8 col-sm-offset-0 col-xs-12 col-xs-offset-0 col-md-6 col-md-offset-2 col-lg-6 col-lg-offset-1 col-global-service .col-rci-description">
				<?php 
					$rci_global_services = get_field('rci_global_services');
					if($rci_global_services): ?>
				<h1 class="rci-global-heading"><?php echo $rci_global_services['title_rcigolbalservices']?></h1>
				<p class="rci-global-description"><?php echo $rci_global_services['sub_rciglobalservices'] ?></p>
				<a id="link">
					<button class="btn-learn-more btn_rsigs" onclick="page1()">LEARN MORE</button>
				</a>
				<script>
						$("#link").click(function() {
  				$('html, body').animate({
    			scrollTop: $("#main").offset().top}, 600);
					});
				</script>
				<?php endif; ?>
			</div>
        <!-- End of RCI Global Services -->
		</div>
	</div>
	<!-- RCI and RSI  -->
	<div class="home_consul_solution">
		<!-- Not hover -->
		<div class="row" style="margin: 0;">
			<div class="col-lg-6 text-center col-rci" onmouseover="myTime = setTimeout(show_rci_hover_header, 1000)" onmouseout="clearTimeout(myTime)">
				<div class="rci-background">
					<?php 
						$consul = get_field('retail_consultion');
						if($consul): ?>
							<p class=" title-rci"><?php echo $consul['title_retailconsulting'] ?></p>
							<h1 class="rci-moto"><?php echo $consul['sub_retailconsulting'] ?></h1>
					<?php endif; ?>
				</div>
			</div>
			<div class="col-lg-6 text-center col-rsi" onmouseover="myTime=setTimeout(show_rsi_hover_header, 1000)" onmouseout="clearTimeout(myTime)">
				<div class="rsi-background">
					<?php 
						$solution = get_field('retail_solution');
						if($solution): ?>
							<p class=" title-rsi"><?php echo $solution['title_retailsolution'] ?></p>
							<h1 class="rsi-moto"><?php echo $solution['sub_retailsolution'] ?></h1>
					<?php endif; ?>
				</div>
			</div>
		</div>
    </div>
	<!-- RCI and RSI On Hover -->
	<div class="row row-home-solution" style="margin: 0;">
		<div class="col-lg-6 col-xs-12 text-center col-rci-hover rci-background-hover" 
			style="transition-delay: 2s;"
			id="rci_hover_header" onmouseout="hide_rci_hover_header()">
			<img id="image-rci-hover" src="wp-content/themes/RCI%20Global/assets/Images/home/u70.png" alt="">
		</div>
		<a href="https://rsiapac.com/" target="_blank">
		<div class="col-lg-6 col-xs-1 text-center col-rsi-hover rsi-background-hover "
			style="transition-delay: 2s;"  
			id="rsi_hover_header" onmouseout="hide_rsi_hover_header()">
				<img id="image-rci-hover" src="wp-content/themes/RCI%20Global/assets/Images/home/u75.png" alt="">
		</div>
		</a>
	</div>
</div>
</div>
<!-- End of RCI and RSI -->
<!-- =========News Center========= -->
<div class="newscenter newscenterhome" id="newscenter_js" style="display: none;">
	<div class="row row-newscenter-panel">
    <div class="col-lg-3 col-xs-12 col-sm-6 col-md-6 col-scol-rgs-heading mobile-heading hidden">
			<div class="row" id="backhome" onclick="homeback()">
				<div class="col-lg-6 col-lg-offset-6 col-back-home">
					<?php
						$home_image = get_field('home_image');
						if($home_image): ?>
						<span class="title_backhome pull-left text-right">BACK TO<br>HOME PAGE</span>
						<div class="image_home_block pull-left">
							<img style="width: 37px; height: 37px; margin-left: 5px;" src="<?php echo $home_image['img_home']['url'];?>" 
							alt="<?php echo $home_image['img_home']['url'];?>">
						</div>
					<?php endif ?>
				</div>
			</div>
			<h1 class="newscenter-heading"><span id="title-r">RCI</span>
			<br><span id="gs">GLOBAL<br>SERVICES</span><br>
			<span id="title-nc">News<br>Center</span></h1>
	</div>
    <div class="col-lg-3 col-xs-12 col-md-6 col-sm-6">
        <div class="map-background">
            <h3 class="service-title inductry">RCI GLOBAL SERVICES <br>inductry news</h3>
        </div>
    </div>
    <div class="clearfix visible-xs "></div>
    <div class="col-lg-3 col-xs-12 col-sm-6 col-md-6">
        <div class="pen-background">
            <h3 class="service-title case-study">RCI GLOBAL SERVICES<br>Case-study</h3>
        </div>
    </div>
    <div class="col-lg-3 col-xs-12 col-sm-6 col-md-6">
        <div class="light-background">
            <h3 class="service-title insight-article">RCI GLOBAL SERVICES<br>insight articles</h3>
        </div>
    </div>
    <div class="col-lg-3 col-xs-12  col-sm-6 col-md-6 col-rgs-heading  desktop-heading">
        <div class="row" id="backhome" onclick="homeback()">
            <div class="col-lg-6 col-lg-offset-6 col-back-home">
                <?php
                    $home_image = get_field('home_image');
                    if($home_image): ?>
                    <span class="title_backhome pull-left text-right">BACK TO<br>HOME PAGE</span>
                    <div class="image_home_block pull-left">
                        <img style="width: 37px; height: 37px; margin-left: 5px;" src="<?php echo $home_image['img_home']['url'];?>" 
                        alt="<?php echo $home_image['img_home']['url'];?>">
                    </div>
                <?php endif ?>
            </div>
        </div>
        <h1 class="newscenter-heading"><span id="title-r">RCI</span>
        <br><span id="gs">GLOBAL<br>SERVICES</span><br>
        <span id="title-nc">News<br>Center</span></h1>
    </div>
	</div>
</div>
<!-- End of News Center -->
<!-- SUB PAGE RCI GLOBAL -->
<div class="rcigs-about page1 hide" id="main"> 
    <!-- About the RCI Global Service Box -->
		<div class="container container_about_page">
			<div class="row">
				<div class="col-xs-12 col-lg-12  text-center col-rsi-rci-about">
					<div class="rci-text-title text-center"> 
						<h1 class="rgs-box-title">About RCI Global Services</h1>
					</div>
					<?php 
						$about_rci = get_field('subpage_rciglobal');
						if($about_rci): ?>
							<div class="col-lg-10 col-lg-offset-1 text-center col-rcigs-about-desciption">
							<img id="image-rci-rsi" src="http://localhost:8089/RCI%20Global/wp-content/uploads/2018/12/r-cirsi.png" alt="">
							<p class="rci-rsi-description"><?php echo $about_rci['title_aboutrci'] ?></p>
							</div>
					<?php endif; ?>
				</div>
			</div>
		</div>
    <!-- End of About the RCI Global Service Box -->
    <!-- Image Branch -->
		<div class="row image-branch" style="margin: 0;">
			<img id="img_regional" src="wp-content/themes/RCI%20Global/assets/Images/branch.png">
		</div>
<!-- by the number	 -->
<div class="row " id="bynumber_js" style="margin: 0;">
	<div class="by_number col-xs-12 col-lg-12">
		<h3 class="title-by-the-numbers">BY THE NUMBERS</h3>
		<div class="row" style="margin: 0;">
		<?php 
			$number = get_field('subpage_rciglobal');
			if($number): ?>
				<div class="container text-center">
					<div class="col-xs-3 col-lg-3">
						<h1 class="number"><?php echo $number['1st'] ?></h1>
						<h3 class="fields"><?php echo $number['oms_cartridge'] ?></h3>
						<p class="field-descriptions"><?php echo $number['in_japan'] ?></p>
					</div>
					<div class="col-xs-1 col-lg-1 col-line">
						<img src="<?php echo $number['rci_liney']['url']; ?>" alt="<?php echo $number['rci_liney']['url']; ?>">
					</div>
					<div class="col-xs-3 col-lg-4 ">
						<h1 class="number"><?php echo $number['6'] ?></h1>
						<h3 class="fields"><?php echo $number['offices'] ?></h3>
						<p class="field-descriptions"><?php echo $number['in_asia'] ?></p>
					</div>
					<div class="col-xs-1 col-lg-1 col-line">
						<img src="<?php echo $number['rci_liney']['url']; ?>" alt="<?php echo $number['rci_liney']['url']; ?>">
					</div>
					<div class="col-xs-3 col-lg-3">
						<h1 class="number"><?php echo $number['10+'] ?></h1>
						<h3 class="fields"><?php echo $number['partners'] ?></h3>
						<p class="field-descriptions"><?php echo $number['well_coorperated'] ?></p>
					</div>
				</div>

					<div class="container text-center">
					<div class="col-xs-3 col-lg-3">
						<h1 class="number"><?php echo $number['20+'] ?></h1>
						<h3 class="fields"><?php echo $number['years'] ?></h3>
						<p class="field-descriptions"><?php echo $number['it_experience'] ?></p>
					</div>
					<div class="col-xs-1 col-lg-1 col-line">
						<img src="<?php echo $number['rci_liney']['url']; ?>" alt="<?php echo $number['rci_liney']['url']; ?>">
					</div>
					<div class="col-xs-3 col-lg-4">
						<h1 class="number"><?php echo $number['40+'] ?></h1>
						<h3 class="fields"><?php echo $number['customers'] ?></h3>
						<p class="field-descriptions"><?php echo $number['worldwide'] ?></p>
					</div>
					<div class="col-xs-1 col-lg-1 col-line">
						<img src="<?php echo $number['rci_liney']['url']; ?>" alt="<?php echo $number['rci_liney']['url']; ?>">
					</div>
					<div class="col-xs-3 col-lg-3">
						<h1 class="number"><?php echo $number['50+'] ?></h1>
						<h3 class="fields"><?php echo $number['projects'] ?></h3>
						<p class="field-descriptions"><?php echo $number['successfully'] ?></p>
					</div>
				</div>
			<?php endif; ?>
		</div>
	</div>
</div>
<!-- enf of by the number -->
<!-- founder	 -->
<div class="container text-center" id="founder">
	<div class="col-xs-12 col-md-8 col-md-offset-2 col-lg-8 col-lg-offset-2">
		<?php 
			$founder = get_field('subpage_rciglobal');
			if($founder): ?>
				<img class="rsi_linex" src="<?php echo $founder['rsi_linex']['url']; ?>" alt="<?php echo $founder['rsi_linex']['url']; ?>">
				<h1 class="titlle-founder"><?php echo $founder['title_founder'] ?></h1>
				<img class="founder-photo" src="<?php echo $founder['img_founder']['url']; ?>" alt="<?php echo $founder['img_founder']['url']; ?>">
				<h3 class="founder-name"><?php echo $founder['jean'] ?></h3>
				<p class="about-founder"><?php echo $founder['title_jean'] ?></p>
				<img class="rsi_linex" src="<?php echo $founder['rsi_linex']['url']; ?>" alt="<?php echo $founder['rsi_linex']['url']; ?>">
			<?php endif; ?>
	</div>
</div>
</div>
<!-- end of founder -->
<?php include 'rci-consulting.php' ;?>
<!-- rsi consulting -->
<?php include 'rsi-solution.php' ;?>
<!-- CUSTOMER -->
<?php include 'customers.php';?>
<!-- RESOURCE-->
<?php include 'resources.php'; ?>
<!-- Contact Page -->
<?php include 'contact.php' ?>
<?php get_footer(); ?>	
<script type="text/javascript">
// resources show panel description
$(document).ready(function(){
	  $("#saleforcePanel").mouseover(function(){
	    $("#saleforceDesc").css("display", "block");
	  });
	  $("#saleforceDesc").mouseout(function(){
	    $("#saleforceDesc").css("display", "none");
	  });
	  $("#magentoPanel").mouseover(function(){
	    $("#magentoDesc").css("display", "block");
	  });
	  $("#magentoDesc").mouseout(function(){
	    $("#magentoDesc").css("display", "none");
	  });
	  $("#omsproPanel").mouseover(function(){
	    $("#omsproDesc").css("display", "block");
	  });
	  $("#omsproDesc").mouseout(function(){
	    $("#omsproDesc").css("display", "none");
	  });
	  $("#retailproPanel").mouseover(function(){
	    $("#retailproDesc").css("display", "block");
	  });
	  $("#retailproDesc").mouseout(function(){
	    $("#retailproDesc").css("display", "none");
	  });
	  $("#oraclePanel").mouseover(function(){
	    $("#oracleDesc").css("display", "block");
	  });
	  $("#oracleDesc").mouseout(function(){
	    $("#oracleDesc").css("display", "none");
	  });
	  $("#ivendPanel").mouseover(function(){
	    $("#ivendDesc").css("display", "block");
	  });
	  $("#ivendDesc").mouseout(function(){
	    $("#ivendDesc").css("display", "none");
	  });
	  $("#venstarPanel").mouseover(function(){
	    $("#venstarDesc").css("display", "block");
	  });
	  $("#venstarDesc").mouseout(function(){
	    $("#venstarDesc").css("display", "none");
	  });
	  $("#accenturePanel").mouseover(function(){
	    $("#accentureDesc").css("display", "block");
	  });
	  $("#accentureDesc").mouseout(function(){
	    $("#accentureDesc").css("display", "none");
	  });
	});

	function saleforcePanel() {
		document.getElementById("saleforceDesc").sytle.display = "block";
	}
	function magentoPanel() {
		document.getElementById("magentoDesc").sytle.display = "block";
	}
	function omsproPanel() {
		document.getElementById("omsproDesc").sytle.display = "block";
	}
	function retailproPanel() {
		document.getElementById("retailproDesc").sytle.display = "block";
	}


	
function show_menu_panel(){
	// $('#menu-background').css("visibility", "visible") ;

	// $('#menu-background').removeClass('hide');
   	// var menu = document.getElementById('menu-background');
   	// 	menu.style.display = 'block';
   	// var item = document.getElementById('menu-item-display');
   	// 	item.style.display = 'block';
    // var background = document.getElementById('menu-background');
    //     background.style.display = 'block';
	$('#menu-background').show();
	$('#menu-item-display').show();
	
   }
//    function hide_menu_panel(){
//    	var menu = document.getElementById('menu-item-display');
//    		menu.style.display = 'none';
//       var item = document.getElementById('menu-item-display');
//       		item.style.display = 'none';
// 	var background = document.getElementById('menu-background');
// 		background.style.display = 'none';
//    }
   function show_rci_hover_header(){
   	var rci_hover_header = document.getElementById('rci_hover_header').style;
   	rci_hover_header.display = 'block';
   	rci_hover_header.cursor = 'pointer';
   }
   function hide_rci_hover_header(){
   	var rci_hover_header = document.getElementById('rci_hover_header').style;
   	rci_hover_header.display = 'none';
   }
   function show_rsi_hover_header(){
   	var rsi_hover_header = document.getElementById('rsi_hover_header').style;
   	rsi_hover_header.display = 'block';
   	rsi_hover_header.marginLeft = '50%';
   	rsi_hover_header.cursor = 'pointer';
   }
   function hide_rsi_hover_header(){
   	var rsi_hover_header = document.getElementById('rsi_hover_header').style;
   	rsi_hover_header.display = 'none';
   }
	  var pageId = '.page0';
      function onClickItem(n){
		$('#menu-background').hide(700);
		$('#menu-item-display').hide(700);

      $('#main').addClass('hide');
	  	$('#learnmore_cus').addClass('hide');
			$('#subresoruce').addClass('hide');
			$('#contacts').addClass('hide');

		 if($(pageId)=='.page0'){
				$(pageId).addClass('hide slideInDown');
				
		 }else
				$(pageId).addClass('hide');
				pageId = n;
				$(pageId).removeClass('hide');
		
				
      }
      // new center
      function newCenter(){
         $(pageId).removeClass('hide', 1000);
         $(pageId).addClass('hide', 1000);
         $('.row-head').css("display", "none", 1000);
         $('.newscenterhome').fadeIn(1000);
         $('.heading').hide();
         $('.menu-icon').hide();
         }
      // back home
      function homeback(){
         $('.row-head').css("display", "block");
         $('.newscenterhome').fadeOut("400");
         $(pageId).removeClass('hide', 1000);
         $('.heading').show();
         $('.menu-icon').show();
      }
      function page1(){
      $('#main').removeClass('hide');
   }
   function learnmore_customer(){
      $('#learnmore_cus').removeClass('hide');
   }
   function subresources(){
      $('.subresoru').removeClass('hide');
   }
   function contact(){
      $('.subcontact').removeClass('hide');
   }
	//  function subresources(){
	// 	 $('#subresoruce').removeClass('hide');
	//  }
</script>