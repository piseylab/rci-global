<div class="blackground">
    <div class="rcigs-page page6 hide slideInUp">
        <div class="contact_background " id="contact_js">
            <div class="main-contact">
                <div class="home_contact img_homecontact row main-description" style="margin-right: 0px;">
                    <div class="col-md-8 col-lg-6 col-lg-offset-1 col-md-6 col-md-offset-2 desc_contact">
                        <?php 
                            $contact = get_field('home_contact');
                            if($homesolution): ?>
                                <h1 class="title_get_started"><?php echo $contact['title_home_contact'] ?></h1>
                                <p class="contact_description"><?php echo $contact['sub_home_contact'] ?></p>
                                <a href="#contacts">
                                    <button class="btn btn-learn-more contact-btn-learn-more btn_contact" 
                                            onclick="contact()">LEARN MORE
                                    </button>
                                </a>
                            <?php endif; ?>
                    </div>
                </div>
                <div class="row" style="margin-right: 0px;">
                    <div class="col-xs-12 col-sm-12 col-md-2 col-md-offset-2 col-lg-5 col-lg-offset-1 col-social">
                        <?php 
                            $img_contact = get_field('home_contact');
                            if($img_contact): ?>
                                <div class="col-xs-3 col-sm-3 col-md-4 col-lg-1">
                                    <img id="image_social" src="<?php echo $img_contact['img_twitter']['url']; ?>" alt="<?php echo $img_contact['img_twitter']['url']; ?>">
                                </div>
                                <div class="col-xs-3 col-sm-3 col-md-4 col-lg-1">
                                    <img id="image_social" src="<?php echo $img_contact['img_facebook']['url']; ?>" alt="<?php echo $img_contact['img_facebook']['url']; ?>">
                                </div>
                                <div class="col-xs-3 col-sm-3 col-md-4 col-lg-1">
                                    <img id="image_social" src="<?php echo $img_contact['img_linkend']['url']; ?>" alt="<?php echo $img_contact['img_linkend']['url']; ?>">
                                </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
        <!-- content load responsive device -->
        <div class="respone-content">
            <div class="container text-decription-responsive">
                <div class="row">
                    <div class="col-sm-12 col-xs-12"></div>
                    <div class="col-xs-12 col-sm-10 col-md-10">
                        <?php 
                            $contact = get_field('home_contact');
                            if($homesolution): ?>
                                <h1 class="title_get_started"><?php echo $contact['title_home_contact'] ?></h1>
                                <p class="contact_description"><?php echo $contact['sub_home_contact'] ?></p>
                                <a href="#contacts">
                                    <button class="btn btn-learn-more contact-btn-learn-more btn_contact" 
                                            onclick="contact()">LEARN MORE
                                    </button>
                                </a>
                        <?php endif; ?>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 icons_social_media">
                        <?php 
                            $img_contact = get_field('home_contact');
                            if($img_contact): ?>
                                <div class="col-xs-2 col-sm-1 col-md-2">
                                    <img id="image_social" src="<?php echo $img_contact['img_twitter']['url']; ?>" alt="<?php echo $img_contact['img_twitter']['url']; ?>">
                                </div>
                                <div class="col-xs-2 col-sm-1 col-md-2">
                                    <img id="image_social" src="<?php echo $img_contact['img_facebook']['url']; ?>" alt="<?php echo $img_contact['img_facebook']['url']; ?>">
                                </div>
                                <div class="col-xs-2 col-sm-1 col-md-2">
                                    <img id="image_social" src="<?php echo $img_contact['img_linkend']['url']; ?>" alt="<?php echo $img_contact['img_linkend']['url']; ?>">
                                </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div> <!-- end responsive -->
    </div>
</div>
<!-- SUB CONTACT -->
<div class="subcontact hide " id="contacts">
    <div class="contact_requ sub_contact_js">
        <div class="backgroundcontact">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-offset-1 col-lg-9 form_contact">
                    <?php echo do_shortcode('[contact-form-7 id="500" title="Contact Form"]');?>
                </div>
        </div>
    </div> 
</div> 