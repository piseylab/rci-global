<div class="blackground">
<div class="rcigs-page page4 hide slideInUp" id="customer_js">
	<div class="home img-customerbackground">
        <div class="main-customer">
            <div class="row main-description " style="margin:0;">
                <div class="col-sm-offset-1 col-sm-6 col-md-offset-2 col-md-6 col-lg-offset-1 col-lg-6 desc_customers col-global-service">
                    <?php 
                    $homecustomer = get_field('home_customer');
                    if($homesolution): ?>
                    <h1 class="title-customer"><?php echo $homecustomer['title_homecustomer'] ?></h1>
                    <p class="rci-customer-description"><?php echo $homecustomer['sub_homecustomer'] ?></p>
                    <a href="#learnmore_cus">
                        <button class="btn-learn-more customer-learn-more-button btn_customer" onclick="learnmore_customer()">LEARN MORE</button>
                    </a>
                        <?php endif; ?>
                </div>
            </div>
            <div class="row row-partner-image">
                    <?php 
                        $img_homecustomer = get_field('home_customer');
                        if($img_homecustomer): ?>
                                <div class="col-xs-6 col-sm-3 col-md-3 col-md-2 col-md-offset-1 col-lg-1 col-lg-offset-0">
                                    <img  id="img-homepandora" src="<?php echo $img_homecustomer['img_homepandora']['url']; ?>" 
                                    alt="<?php echo $img_homecustomer['img_homepandora']['url']; ?>">
                                </div>
                                <div class="col-xs-6 col-sm-3 col-md-1 col-lg-1 text-center">
                                    <img  id="img-homelv" src="<?php echo $img_homecustomer['img_homelv']['url']; ?>" 
                                    alt="<?php echo $img_homecustomer['img_homelv']['url']; ?>">
                                </div>
                                <div class="col-xs-6 col-sm-3 col-md-2 col-lg-1">
                                    <img id="img-homegucci" src="<?php echo $img_homecustomer['img_homegucci']['url']; ?>" 
                                    alt="<?php echo $img_homecustomer['img_homegucci']['url']; ?>">
                                </div>
                                <div class="col-xs-6 col-sm-3 col-md-3 col-lg-2 text-center">
                                    <img id="img-homeloreal"src="<?php echo $img_homecustomer['img_homeloreal']['url']; ?>" 
                                    alt="<?php echo $img_homecustomer['img_homeloreal']['url']; ?>">
                                </div>
                    <?php endif; ?>
            </div>
        </div>
        <!-- end of row bottom image -->
    </div>
    <!-- content load responsive device -->
    <div class="respone-content">
		<div class="container text-decription-responsive">
			<div class="row">
				<div class="col-sm-12 col-xs-12"></div>
				<div class="col-sm-12 col-xs-12">
                    <?php 
                        $homecustomer = get_field('home_customer');
                        if($homesolution): ?>
                            <h1 class="title-customer"><?php echo $homecustomer['title_homecustomer'] ?></h1>
                            <p class="rci-customer-description"><?php echo $homecustomer['sub_homecustomer'] ?></p>
                            <a href="#learnmore_cus">
                                <button class="btn-learn-more customer-learn-more-button btn_customer" onclick="learnmore_customer()">LEARN MORE</button>
                            </a>
                    <?php endif; ?>
				</div>
				<div class="col-sm-12 col-xs-12" style="padding-top: 7%;">
                    <?php 
                        $img_homecustomer = get_field('home_customer');
                        if($img_homecustomer): ?>
                                <div class="col-xs-6 col-sm-3 col-md-3 col-lg-1">
                                    <img  id="img-homepandora" src="<?php echo $img_homecustomer['img_homepandora']['url']; ?>" 
                                    alt="<?php echo $img_homecustomer['img_homepandora']['url']; ?>">
                                </div>
                                <div class="col-xs-6 col-sm-3 col-md-3 col-lg-1">
                                    <img  id="img-homelv" src="<?php echo $img_homecustomer['img_homelv']['url']; ?>" 
                                    alt="<?php echo $img_homecustomer['img_homelv']['url']; ?>">
                                </div>
                                <div class="col-xs-6 col-sm-3 col-md-3 col-lg-2">
                                    <img id="img-homegucci" src="<?php echo $img_homecustomer['img_homegucci']['url']; ?>" 
                                    alt="<?php echo $img_homecustomer['img_homegucci']['url']; ?>">
                                </div>
                                <div class="col-xs-6 col-sm-3 col-md-3 col-lg-1">
                                    <img id="img-homeloreal"src="<?php echo $img_homecustomer['img_homeloreal']['url']; ?>" 
                                    alt="<?php echo $img_homecustomer['img_homeloreal']['url']; ?>">
                                </div>
                    <?php endif; ?>
                </div>
			</div>
		</div>
	</div>
</div>
</div>
<!-- customer subpage -->
<!-- sub customer -->
<div class="sub_customers hide" id="learnmore_cus">
<div class="container sub_customer_js" id="customer_js">
    <!-- logo and title -->
		<div class="row row-logo-tile">
			<div class="col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4 col-lg-4 col-lg-offset-4 text-center">
				<?php 
					$sub_customer = get_field('subpage_customer');
					if($sub_customer):  ?>
						<img style="width: 100%;" src="<?php echo $sub_customer['img_rciandrsi']['url']; ?>" alt="<?php echo $sub_customer['img_rciandrsi']['url']; ?>">
						<h1 class="maintitle-customer"><?php echo $sub_customer['title_subpage_cus'] ?></h1>
						<p class="text-customer-description"><?php echo $sub_customer['sub_subpage_cu'] ?></p>
					<?php endif; ?>
			</div>
        </div>
        <!-- end of logo and title -->
        <!-- heading images -->
		<div class="row row-heading-images">
			<?php 
				$imgs_customer = get_field('subpage_customer');
				if($imgs_customer):	 ?>
					<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
						<img id="img_loreal_cus"src="<?php echo $imgs_customer['img_loreal_cus']['url'] ?>" alt="<?php echo $imgs_customer['img_loreal_cus']['url'] ?>">
					</div>
					<div  class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
						<img id="img_kering_cus" src="<?php echo $imgs_customer['img_kering_cus']['url'] ?>" alt="<?php echo $imgs_customer['img_kering_cus']['url'] ?>">
					</div>
					<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
						<img id="img_lvmh_cus" src="<?php echo $imgs_customer['img_lvmh_cus']['url'] ?>" alt="<?php echo $imgs_customer['img_lvmh_cus']['url'] ?>">
					</div>
					<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
						<img id="img_pandora_cus" src="<?php echo $imgs_customer['img_pandora_cus']['url'] ?>" alt="<?php echo $imgs_customer['img_pandora_cus']['url'] ?>">
					</div>
				<?php endif; ?>
        </div>
        <!-- end of heading images -->
	</div>
    <!-- line1 -->
	<div class="container sub_customer_js" id="customer_js">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<?php 
				$group_image = get_field('subpage_customer');
				if($group_image):	 ?>
					<div class="col-xs-4 col-sm-2 col-md-1 col-lg-1 customer_border">
							<img id="image-group" src="<?php echo $group_image['line1_img1']['url']; ?>" alt="<?php echo $group_image['line1_img1']['url']; ?>">
					</div>
					<div  class="col-xs-4 col-sm-2 col-md-1 col-lg-1 customer_border">
							<img id="image-group" src="<?php echo $group_image['line1_img2']['url']; ?>" alt="<?php echo $group_image['line1_img2']['url']; ?>">
					</div>
					<div class="col-xs-4 col-sm-2 col-md-1 col-lg-1 customer_border">
							<img  id="image-group" src="<?php echo $group_image['line1_img3']['url']; ?>" alt="<?php echo $group_image['line1_img3']['url']; ?>">
					</div>
					<div class="col-xs-4 col-sm-2 col-md-1 col-lg-1 customer_border">
							<img  id="image-group" src="<?php echo $group_image['line1_img4']['url']; ?>" alt="<?php echo $group_image['line1_img4']['url']; ?>">
					</div>
					<div class="col-xs-4 col-sm-2 col-md-1 col-lg-1 customer_border">
							<img  id="image-group" src="<?php echo $group_image['line1_img5']['url']; ?>" alt="<?php echo $group_image['line1_img5']['url']; ?>">
					</div>
					<div class="col-xs-4 col-sm-2 col-md-1 col-lg-1 customer_border">
							<img  id="image-group" src="<?php echo $group_image['line1_img6']['url']; ?>" alt="<?php echo $group_image['line1_img6']['url']; ?>">
					</div>
					<div class="col-xs-4 col-sm-2 col-md-1 col-lg-1 customer_border">
							<img  id="image-group" src="<?php echo $group_image['line1_img7']['url']; ?>" alt="<?php echo $group_image['line1_img7']['url']; ?>">
					</div>
					<div class="col-xs-4 col-sm-2 col-md-1 col-lg-1 customer_border">
							<img  id="image-group" src="<?php echo $group_image['line1_img8']['url']; ?>" alt="<?php echo $group_image['line1_img8']['url']; ?>">
					</div>
					<div class="col-xs-4 col-sm-2 col-md-1 col-lg-1 customer_border">
							<img id="image-group" src="<?php echo $group_image['line1_img9']['url']; ?>" alt="<?php echo $group_image['line1_img9']['url']; ?>">
					</div>
					<div class="col-xs-4 col-sm-2 col-md-1 col-lg-1 customer_border">
							<img id="image-group" src="<?php echo $group_image['line1_img10']['url']; ?>" alt="<?php echo $group_image['line1_img10']['url']; ?>">
					</div>
					<div class="col-xs-4 col-sm-2 col-md-1 col-lg-1 customer_border">
							<img id="image-group" src="<?php echo $group_image['line1_img11']['url']; ?>" alt="<?php echo $group_image['line1_img11']['url']; ?>">
					</div>
					<div class="col-xs-4 col-sm-2 col-md-1 col-lg-1 customer_border">
							<img id="image-group" src="<?php echo $group_image['line1_img12']['url']; ?>" alt="<?php echo $group_image['line1_img12']['url']; ?>">
					</div>
				<?php endif; ?>
        </div>
        <!-- end of line1 -->
        <!-- line2 -->
     <?php 
        $img_line2= get_field('subpage_customer_contiune');
        if($img_line2): ?>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="col-xs-4 col-sm-2 col-md-1 col-lg-1 customer_border">
                    <img id="image-group" src="<?php echo $img_line2['line2_img1']['url']; ?>" alt="<?php echo $img_line2['line2_img1']['url']; ?>">
                </div>
                <div class="col-xs-4 col-sm-1 col-md-1 col-lg-1  customer_border">
                    <img id="image-group"  src="<?php echo $img_line2['line2_img2']['url']; ?>" alt="<?php echo $img_line2['line2_img2']['url']; ?>">
                </div>
                <div class="col-xs-4 col-sm-2 col-md-1 col-lg-1 customer_border">
                    <img  id="image-group" src="<?php echo $img_line2['line2_img3']['url']; ?>" alt="<?php echo $img_line2['line2_img3']['url']; ?>">
                </div>
                <div class="col-xs-4 col-sm-2 col-md-1 col-lg-1 customer_border">
                    <img  id="image-group" src="<?php echo $img_line2['line2_img4']['url']; ?>" alt="<?php echo $img_line2['line2_img4']['url']; ?>">
                </div>
                <div class="col-xs-4 col-sm-2 col-md-1 col-lg-1 customer_border">
                    <img  id="image-group" src="<?php echo $img_line2['line2_img5']['url']; ?>" alt="<?php echo $img_line2['line2_img5']['url']; ?>">
                </div>
                <div class="col-xs-4 col-sm-2 col-md-1 col-lg-1 customer_border">
                    <img   id="image-group" src="<?php echo $img_line2['line2_img6']['url']; ?>" alt="<?php echo $img_line2['line2_img6']['url']; ?>">
                </div>
                <div class="col-xs-4 col-sm-2 col-md-1 col-lg-1 customer_border">
                    <img   id="image-group" src="<?php echo $img_line2['line2_img7']['url']; ?>" alt="<?php echo $img_line2['line2_img7']['url']; ?>">
                </div>
                <div class="col-xs-4 col-sm-2 col-md-1 col-lg-1 customer_border">
                    <img   id="image-group" src="<?php echo $img_line2['line2_img8']['url']; ?>" alt="<?php echo $img_line2['line2_img8']['url']; ?>">
                </div>
                <div class="col-xs-4 col-sm-2 col-md-1 col-lg-1 customer_border">
                    <img   id="image-group" src="<?php echo $img_line2['line2_img9']['url']; ?>" alt="<?php echo $img_line2['line2_img9']['url']; ?>">
                </div>
                <div class="col-xs-4 col-sm-2 col-md-1 col-lg-1 customer_border">
                    <img   id="image-group" src="<?php echo $img_line2['line2_img10']['url']; ?>" alt="<?php echo $img_line2['line2_img10']['url']; ?>">
                </div>
                <div class="col-xs-4 col-sm-2 col-md-1 col-lg-1 customer_border">
                    <img  id="image-group" src="<?php echo $img_line2['line2_img11']['url']; ?>" alt="<?php echo $img_line2['line2_img11']['url']; ?>">
                </div>
                <div class="col-xs-4 col-sm-2 col-md-1 col-lg-1 customer_border">
                    <img id="image-group" src="<?php echo $img_line2['line2_img12']['url']; ?>" alt="<?php echo $img_line2['line2_img12']['url']; ?>">
                </div>
            </div>
        <?php endif; ?>
        <!-- line3 -->
        <?php 
            $img_line3= get_field('subpage_customer_con1');
            if($img_line3): ?>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" >
                    <div class="col-xs-4 col-sm-2 col-md-1 col-lg-1 customer_border">
                        <img id="image-group" src="<?php echo $img_line3['line3_img1']['url']; ?>" alt="<?php echo $img_line3['line3_img1']['url']; ?>">
                    </div>
                    <div class="col-xs-4 col-sm-2 col-md-1 col-lg-1 customer_border">
                        <img  id="image-group" src="<?php echo $img_line3['line3_img2']['url']; ?>" alt="<?php echo $img_line3['line3_img2']['url']; ?>">
                    </div>
                    <div class="col-xs-4 col-sm-2 col-md-1 col-lg-1 customer_border">
                        <img  id="image-group" src="<?php echo $img_line3['line3_img3']['url']; ?>" alt="<?php echo $img_line3['line3_img3']['url']; ?>">
                    </div>
                    <div class="col-xs-4 col-sm-2 col-md-1 col-lg-1 customer_border">
                        <img  id="image-group" src="<?php echo $img_line3['line3_img4']['url']; ?>" alt="<?php echo $img_line3['line3_img4']['url']; ?>">
                    </div>
                    <div class="col-xs-4 col-sm-2 col-md-1 col-lg-1 customer_border">
                        <img  id="image-group" src="<?php echo $img_line3['line3_img5']['url']; ?>" alt="<?php echo $img_line3['line3_img5']['url']; ?>">
                    </div>
                    <div class="col-xs-4 col-sm-2 col-md-1 col-lg-1 customer_border">
                        <img  id="image-group" src="<?php echo $img_line3['line3_img6']['url']; ?>" alt="<?php echo $img_line3['line3_img6']['url']; ?>">
                    </div>
                    <div class="col-xs-4 col-sm-2 col-md-1 col-lg-1 customer_border">
                        <img  id="image-group" src="<?php echo $img_line3['line3_img7']['url']; ?>" alt="<?php echo $img_line3['line3_img7']['url']; ?>">
                    </div>
                    <div class="col-xs-4 col-sm-2 col-md-1 col-lg-1 customer_border">
                        <img  id="image-group" src="<?php echo $img_line3['line3_img8']['url']; ?>" alt="<?php echo $img_line3['line3_img8']['url']; ?>">
                    </div>
                    <div class="col-xs-4 col-sm-2 col-md-1 col-lg-1 customer_border">
                        <img  id="image-group" src="<?php echo $img_line3['line3_img9']['url']; ?>" alt="<?php echo $img_line3['line3_img9']['url']; ?>">
                    </div>
                    <div class="col-xs-4 col-sm-2 col-md-1 col-lg-1 customer_border">
                        <img  id="image-group" src="<?php echo $img_line3['line3_img10']['url']; ?>" alt="<?php echo $img_line3['line3_img10']['url']; ?>">
                    </div>
                    <div class="col-xs-4 col-sm-2 col-md-1 col-lg-1 customer_border">
                        <img id="image-group" src="<?php echo $img_line3['line3_img11']['url']; ?>" alt="<?php echo $img_line3['line3_img11']['url']; ?>">
                    </div>
                    <div class="col-xs-4 col-sm-2 col-md-1 col-lg-1 customer_border">
                        <img id="image-group" src="<?php echo $img_line3['line3_img12']['url']; ?>" alt="<?php echo $img_line3['line3_img12']['url']; ?>">
                    </div>
                </div>
            <?php endif; ?>
            <!-- line4 -->
				<?php 
                $img_line4= get_field('subpage_customer_con2');
                if($img_line4): ?>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="col-xs-4 col-sm-2 col-md-1 col-lg-1 customer_border">
                            <img id="image-group" src="<?php echo $img_line4['line4_img1']['url']; ?>" alt="<?php echo $img_line4['line4_img1']['url']; ?>">
                        </div>
                        <div class="col-xs-4 col-sm-2 col-md-1 col-lg-1 customer_border">
                            <img  id="image-group" src="<?php echo $img_line4['line4_img2']['url']; ?>" alt="<?php echo $img_line4['line4_img2']['url']; ?>">
                        </div>
                        <div class="col-xs-4 col-sm-2 col-md-1 col-lg-1 customer_border">
                            <img  id="image-group" src="<?php echo $img_line4['line4_img3']['url']; ?>" alt="<?php echo $img_line4['line4_img3']['url']; ?>">
                        </div>
                        <div class="col-xs-4 col-sm-2 col-md-1 col-lg-1 customer_border">
                            <img  id="image-group" src="<?php echo $img_line4['line4_img4']['url']; ?>" alt="<?php echo $img_line4['line4_img4']['url']; ?>">
                        </div>
                        <div class="col-xs-4 col-sm-2 col-md-1 col-lg-1 customer_border">
                            <img  id="image-group"src="<?php echo $img_line4['line4_img5']['url']; ?>" alt="<?php echo $img_line4['line4_img5']['url']; ?>">
                        </div>
                        <div class="col-xs-4 col-sm-2 col-md-1 col-lg-1 customer_border">
                            <img  id="image-group" src="<?php echo $img_line4['line4_img6']['url']; ?>" alt="<?php echo $img_line4['line4_img6']['url']; ?>">
                        </div>
                        <div class="col-xs-4 col-sm-2 col-md-1 col-lg-1 customer_border">
                            <img  id="image-group"src="<?php echo $img_line4['line4_img7']['url']; ?>" alt="<?php echo $img_line4['line4_img7']['url']; ?>">
                        </div>
                        <div class="col-xs-4 col-sm-2 col-md-1 col-lg-1 customer_border">
                            <img  id="image-group" src="<?php echo $img_line4['line4_img8']['url']; ?>" alt="<?php echo $img_line4['line4_img8']['url']; ?>">
                        </div>
                        <div class="col-xs-4 col-sm-2 col-md-1 col-lg-1 customer_border">
                            <img  id="image-group" src="<?php echo $img_line4['line4_img9']['url']; ?>" alt="<?php echo $img_line4['line4_img9']['url']; ?>">
                        </div>
                        <div class="col-xs-4 col-sm-2 col-md-1 col-lg-1 customer_border">
                            <img  id="image-group" src="<?php echo $img_line4['line4_img10']['url']; ?>" alt="<?php echo $img_line4['line4_img10']['url']; ?>">
                        </div>
                        <div class="col-xs-4 col-sm-2 col-md-1 col-lg-1 customer_border">
                            <img id="image-group" src="<?php echo $img_line4['line4_img11']['url']; ?>" alt="<?php echo $img_line4['line4_img11']['url']; ?>">
                        </div>
                        <div class="col-xs-4 col-sm-2 col-md-1 col-lg-1 customer_border">
                            <img id="image-group" src="<?php echo $img_line4['line4_img12']['url']; ?>" alt="<?php echo $img_line4['line4_img12']['url']; ?>">
                        </div>
                    </div>
                <?php endif; ?>
				<!-- end line4 -->
    </div>

<!-- case study -->
    <div class="container sub_customer_js ">
        <div class="row row-case-study">
            <div class="col-lg-12 text-center">
                <?php $casestudy = get_field('subpage_customer_casestudy'); 
                if($casestudy): ?>
                    <h1 class="maintitle-customer"><?php echo $casestudy['title_casestudy_cus'] ?></h1>
                    <p class="text-customer-description"><?php echo $casestudy['sub_casestudy_cus'] ?></p>
            </div>
            <div class="banner_image">
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 border_pandora">
                        <div class="background_pandora">
                            <img id="casestudy_pandora" src="<?php echo $casestudy['casestudy_pandora']['url']; ?>" alt="<?php echo $casestudy['casestudy_pandora']['url']; ?>">
                        </div>
                        <div class="case_pandora text-left">
                            <h2 class="titleof-casestudy"><?php echo $casestudy['title_casepandora'] ?></h2>
                            <p class="descriptionof-casestudy"><?php echo $casestudy['sub_casepandora'] ?></p>
                            <a class="anchor-read-full" href="https://rsiapac.com/customers/pandora-case/"><button class="btn btn-read-full">Read the full article</button></a>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 border_pandora">
                        <div class="background_pandora">
                            <img id="casestudy_loreal" src="<?php echo $casestudy['casestudy_loreal']['url']; ?>" alt="<?php echo $casestudy['casestudy_loreal']['url']; ?>">
                        </div>
                        <div class="case_pandora text-left">
                            <h2 class="titleof-casestudy"><?php echo $casestudy['title_caseloreal'] ?></h2>
                            <p class="descriptionof-casestudy"><?php echo $casestudy['sub_caseloreal'] ?></p>
                            <a class="anchor-read-full" href="https://rsiapac.com/customers/loreal-case/"><button class="btn btn-read-full">Read the full article</button></a>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 border_pandora">
                        <div class="background_pandora">
                            <img id="casestudy_innisfree" src="<?php echo $casestudy['casestudy_innisfree']['url']; ?>" alt="<?php echo $casestudy['casestudy_innisfree']['url']; ?>">
                        </div>
                        <div class="case_pandora text-left">
                            <h2 class="titleof-casestudy"><?php echo $casestudy['title_caseloreal'] ?></h2>
                            <p class="descriptionof-casestudy"><?php echo $casestudy['sub_caseloreal'] ?></p>
                            <a class="anchor-read-full" href="https://rsiapac.com/customers/innisfree-case/"><button class="btn btn-read-full">Read the full article</button></a>
                        </div>
                    </div>
            
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 border_pandora">
                        <div class="background_pandora">
                            <img id="casestudy_celine" src="<?php echo $casestudy['casestudy_celine']['url']; ?>" alt="<?php echo $casestudy['casestudy_celine']['url']; ?>">
                        </div>
                        <div class="case_pandora text-left">
                            <h2 class="titleof-casestudy"><?php echo $casestudy['title_caseceline'] ?></h2>
                            <p class="descriptionof-casestudy"><?php echo $casestudy['sub_caseceline'] ?></p>
                            <a class="anchor-read-full" href="<?php echo $casestudy['link_caseceline'] ;?>"><button class="btn btn-read-full">Read the full article</button></a>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 border_pandora">
                        <div class="background_pandora">
                            <img id="casestudy_gucci" src="<?php echo $casestudy['casestudy_gucci']['url']; ?>" alt="<?php echo $casestudy['casestudy_gucci']['url']; ?>">
                        </div>
                        <div class="case_pandora text-left">
                            <h2 class="titleof-casestudy"><?php echo $casestudy['title_casegucci'] ?></h2>
                            <p class="descriptionof-casestudy"><?php echo $casestudy['sub_casegucii'] ?></p>
                            <a class="anchor-read-full" href="<?php echo $casestudy['link_casegucii'] ;?>"><button class="btn btn-read-full">Read the full article</button></a>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 border_pandora">
                        <div class="background_pandora">
                            <img id="casestudy_fendi" src="<?php echo $casestudy['casestudy_fendi']['url']; ?>" alt="<?php echo $casestudy['casestudy_fendi']['url']; ?>">
                        </div>
                        <div class="case_pandora text-left">
                            <h2 class="titleof-casestudy"><?php echo $casestudy['title_casefendi'] ?></h2>
                            <p class="descriptionof-casestudy"><?php echo $casestudy['sub_casefendi'] ?></p>
                            <a class="anchor-read-full" href="<?php echo $casestudy['link_casefendi'] ;?>"><button class="btn btn-read-full">Read the full article</button></a>
                        </div>
                    </div>
            </div>
            <?php endif; ?>
        </div>
    </div>
</div>