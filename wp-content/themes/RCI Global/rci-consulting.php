
<div class="blackground">
    <div class="rcigs-page page2 slideInUp hide" id="rci_consulting_js">
        <div class="home img_backconsulting ">
        <div class="image-on-rci hidden">
            <img id="image-press-phone"src="wp-content/themes/RCI%20Global/assets/Images/u77.jpg" alt="">
        </div>
        <!-- rci consulting -->
            <div class="row main-description" style="margin:0;">
                <div class="col-xs-12">
                    <img id="background_rci_consulting" src="http://localhost:8089/RCI%20Global/wp-content/uploads/2019/01/u77.jpg" alt="">
                </div>
                <div class="col-sm-8 col-sm-offset-2 col-lg-6 col-lg-offset-1 col-md-6 col-md-offset-2 col-rci-descript">
                    <?php 
                        $homeconsulting = get_field('rci_consulting_path');
                        if($homeconsulting): ?>
                    <p class="rci-consulting-description"><?php echo $homeconsulting['path_title_rciconsulting'] ?></p>
                    <a class="anchor-learn-more" href="<?php echo $homeconsulting['path_link_rciconsulting'] ?>">
                        <button class="btn-learn-more rci-learn-more-button btn_rciconsulting">LEARN MORE</button>
                    </a>
                    <?php endif; ?>
                </div>
            </div>
            <!-- the bottom row with images  -->
            <div class="row row-rci-bottom">
                    <!-- left rci logo -->
                    <div class="col-lg-4 col-sm-6 col-md-6 col-rci-img-group">
                        <img id="img-rciconsulting" src="http://localhost:8089/RCI%20Global/wp-content/uploads/2019/01/Picture11.png" alt="">
                        <!-- <img id= "image-blue-triangle" src="http://localhost:8089/RCI%20Global/wp-content/uploads/2019/01/u85.png" alt=""> -->
                    </div>
                    <!-- end of left rci logo -->
                    <!-- right image group -->
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-8 col-image-with-title">
                        <?php 
                            $rci_detail = get_field('rci_consulting_path');
                            if($rci_detail): ?>
                                <div class="row row-technology" >
                                    <div class="col-xs-12 col-lg-4 text-left img_techno">
                                            <div class="image-border">
                                                <img id="img-technology" src="<?php echo $rci_detail['img_technology']['url']; ?>" alt="<?php echo $rci_detail['img_technology']['url']; ?>">
                                            </div>
                                            <p class="img-title technology-title"><?php echo $rci_detail['technology'] ?></p>
                                    </div>
                                    <div class="col-xs-12 col-lg-6 text-left">
                                        <div class="image-border">
                                            <img id="img-omnichannel" src="<?php echo $rci_detail['img_omnichannel']['url']; ?>" alt="<?php echo $rci_detail['img_omnichannel']['url']; ?>">
                                        </div>	
                                        <p class="img-title omnichannel-title"><?php echo $rci_detail['title_omnichannel'] ?></p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-lg-4 text-left img_etail">
                                        <div class="image-border">
                                            <img id="img-etailing" src="<?php echo $rci_detail['img_etailing']['url']; ?>" alt="<?php echo $rci_detail['img_etailing']['url']; ?>">
                                        </div>
                                        <p class="img-title e-tail-titile"><?php echo $rci_detail['title_etailing'] ?></p>
                                    </div>
                                    <div class="col-xs-12 col-lg-6 text-left">
                                        <div class="image-border">
                                            <img id="img-advance" src="<?php echo $rci_detail['img_advance']['url']; ?>" alt="<?php echo $rci_detail['img_advance']['url']; ?>">
                                        </div>	
                                        <p class="img-title advnace-title"><?php echo $rci_detail['title_avance'] ?></p>
                                    </div>
                                </div>
                        <?php endif; ?>
                    </div>
                    <!-- end of right image group -->
                </div>
            <!-- end of the bottom row with images -->
        </div>
    </div>
</div>