<div class="blackground">
	<div class="rcigs-page page5 hide slideInUp" >
		<div class="resource_background " id="resources_js">
			<div class="main-resources">
				<div class="row home_resource img_homeresources main-description" style="margin: 0;">
					<div class="col-sm-6 col-sm-offset-1 col-md-6 col-md-offset-2 col-lg-6 col-lg-offset-1 col-resources-description">
						<?php 
							$home_resources = get_field('home_resources');
							if($homesolution): ?>
								<h1 class="resource_heading"><?php echo $home_resources['title_homeresources'] ?></h1>
								<p class="resource_description"><?php echo $home_resources['sub_homeresources'] ?></p>
								<a href="#subresoruce">
									<button class="btn btn-learn-more resource-btn-learn-more btn_resources" 
									onclick="subresources()">LEARN MORE</button>
								</a>
						<?php endif; ?>
					</div>
				</div>
				<div class="row row-bottom-image" style="margin: 0">
					<div class="col-sm-8 col-sm-offset-1 col-md-12 col-md-offset-1 col-lg-8 col-lg-offset-1 col-bottom-image">
						<?php 
							$img_resources = get_field('home_resources');
							if($img_resources): ?>
								<div class="col-xs-6 col-sm-4 col-md-3 col-md-offset-1 col-lg-2 resource_image_border">
									<img style="width: 105px; height: 55px;" src="<?php echo $img_resources['img_commerce_cloud']['url']; ?>" alt="<?php echo $img_resources['img_commerce_cloud']['url']; ?>">
								</div>
								<div class="col-xs-6 col-sm-3 col-md-3 col-lg-2 resource_image_border">
									<img style="width: 94px; height: 46px;" src="<?php echo $img_resources['img_magento']['url']; ?>" alt="<?php echo $img_resources['img_magento']['url']; ?>">
								</div>
								<div class="col-xs-6 col-sm-3 col-md-3 col-lg-2 resource_image_border">
									<img style="width: 92px; height: 45px;" src="<?php echo $img_resources['img_oms_pro']['url']; ?>" alt="<?php echo $img_resources['img_oms_pro']['url']; ?>">
								</div>
								<div class="col-xs-6 col-sm-3 col-md-3 col-lg-2 resource_image_border">
									<img style="width: 102px; height: 43px;" src="<?php echo $img_resources['img_oracle']['url']; ?>" alt="<?php echo $img_resources['img_oracle']['url']; ?>">
								</div>
								<div class="col-xs-6 col-sm-3 col-md-3 col-lg-2 resource_image_border">
									<img style="width: 72px; height: 49px;" src="<?php echo $img_resources['img_retailpro']['url']; ?>" alt="<?php echo $img_resources['img_retailpro']['url']; ?>">
								</div>
								<div class="col-xs-6 col-sm-3 col-md-3 col-lg-2 resource_image_border">
									<img style="width: 88px; height: 66px;" src="<?php echo $img_resources['img_accenture']['url']; ?>" alt="<?php echo $img_resources['img_accenture']['url']; ?>">
								</div>
						<?php endif; ?>
					</div>
				</div>
			</div>	
			</div>
			<!-- content load responsive device -->
			<div class="respone-content">
				<div class="container text-decription-responsive">
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12"></div>
						<div class="col-xs-12 col-sm-10 col-md-12">
							<?php 
								$home_resources = get_field('home_resources');
								if($homesolution): ?>
									<h1 class="resource_heading"><?php echo $home_resources['title_homeresources'] ?></h1>
									<p class="resource_description"><?php echo $home_resources['sub_homeresources'] ?></p>
									<a href="#subresoruce">
										<button class="btn btn-learn-more resource-btn-learn-more btn_resources" 
										onclick="subresources()">LEARN MORE</button>
									</a>
							<?php endif; ?>
						</div>
						<div class="col-xs-12 col-sm-12 col-md-12" style="padding-left: 3%; padding-top: 5%;">
							<?php 
								$img_resources = get_field('home_resources');
								if($img_resources): ?>
									<div class="col-xs-6 col-sm-4 col-md-3 resource_image_border">
										<img style="width: 105px; height: 55px;" src="<?php echo $img_resources['img_commerce_cloud']['url']; ?>" alt="<?php echo $img_resources['img_commerce_cloud']['url']; ?>">
									</div>
									<div class="col-xs-6 col-sm-3 col-md-3 resource_image_border">
										<img style="width: 94px; height: 46px;" src="<?php echo $img_resources['img_magento']['url']; ?>" alt="<?php echo $img_resources['img_magento']['url']; ?>">
									</div>
									<div class="col-xs-6 col-sm-3 col-md-3 resource_image_border">
										<img style="width: 92px; height: 45px;" src="<?php echo $img_resources['img_oms_pro']['url']; ?>" alt="<?php echo $img_resources['img_oms_pro']['url']; ?>">
									</div>
									<div class="col-xs-6 col-sm-3 col-md-3 resource_image_border">
										<img style="width: 102px; height: 43px;" src="<?php echo $img_resources['img_oracle']['url']; ?>" alt="<?php echo $img_resources['img_oracle']['url']; ?>">
									</div>
									<div class="col-xs-6 col-sm-3 col-md-3 resource_image_border">
										<img style="width: 72px; height: 49px;" src="<?php echo $img_resources['img_retailpro']['url']; ?>" alt="<?php echo $img_resources['img_retailpro']['url']; ?>">
									</div>
									<div class="col-xs-6 col-sm-3 col-md-3 resource_image_border">
										<img style="width: 88px; height: 66px;" src="<?php echo $img_resources['img_accenture']['url']; ?>" alt="<?php echo $img_resources['img_accenture']['url']; ?>">
									</div>
							<?php endif; ?>
						</div>
					</div>
				</div>
		</div>
	</div>
</div>

<!-- SUBPAGE RESOURCES -->
<div class="subresoru hide" id="subresoruce" >
<div class="container text-center sbupage_resources sub_resources_js">
	<?php 
		$sub_resource = get_field('sbupage_resources');
		if($sub_resource): ?>
			<div class="col-lg-12">
				<img src="<?php echo $sub_resource['resource_rciandrsi']['url']; ?>" alt="<?php echo $sub_resource['resource_rciandrsi']['url']; ?>">
				<h1 id="title-resource"><?php echo $sub_resource['subpage_titleresources'] ?></h1>
				<p id="text-resource-partner"><?php echo $sub_resource['subpage_subresources'] ?></p>
			</div>
			<div class="col-lg-12">
				<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 border_resources"  id="saleforcePanel" ontouchstart="saleforcePanel()">
					<img id="group_imglogo" src="<?php echo $sub_resource['res_salesforce']['url']; ?>" alt="<?php echo $sub_resource['res_salesforce']['url']; ?>">
				</div>
				<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 border_resources" id="magentoPanel" ontouchstart="magentoPanel()">
					<img  id="group_imglogo" src="<?php echo $sub_resource['res_magento']['url']; ?>" alt="<?php echo $sub_resource['res_magento']['url']; ?>">
				</div>
				<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 border_resources" id="omsproPanel" ontouchstart="omsproPanel()">
					<img id="group_imglogo" src="<?php echo $sub_resource['res_omspro']['url']; ?>" alt="<?php echo $sub_resource['res_omspro']['url']; ?>">
				</div>
				<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 border_resources" id="retailproPanel" ontouchstart="retailproPanel()">
					<img  id="group_imglogo" src="<?php echo $sub_resource['res_retailpro']['url']; ?>" alt="<?php echo $sub_resource['res_retailpro']['url']; ?>">
				</div>
			</div>
			<!-- Description row 1-->
			<div class="col-lg-12">
				<div class="col-xs-12 col-sm-3 col-md-3 col-lg-6 desc_panels" id="saleforceDesc">
					<img id="group_imgdescLeft" src="<?php echo $sub_resource['res_salesforce']['url']; ?>" alt="<?php echo $sub_resource['res_salesforce']['url']; ?>">
					<p id="desc-resource-panel"><?php echo $sub_resource['desc_saleforce'] ?></p>
				</div>
				<div class="col-xs-12 col-sm-3 col-md-3 col-lg-offset-3 col-lg-6 desc_panels" id="magentoDesc" >
					<img id="group_imgdescLeft" src="<?php echo $sub_resource['res_magento']['url']; ?>" alt="<?php echo $sub_resource['res_magento']['url']; ?>">
					<p id="desc-resource-panel"><?php echo $sub_resource['desc_magento'] ?></p>
				</div>
				<div class="col-xs-12 col-sm-3 col-md-3 col-lg-offset-3 col-lg-6 desc_panels" id="omsproDesc">
					<img id="group_imgdescRight" src="<?php echo $sub_resource['res_omspro']['url']; ?>" alt="<?php echo $sub_resource['res_omspro']['url']; ?>">
					<p id="desc-resource-panel"><?php echo $sub_resource['desc_omspro'] ?></p>
				</div>
				<div class="col-xs-12 col-sm-3 col-md-3 col-lg-offset-6 col-lg-6 desc_panels" id="retailproDesc">
					<img id="group_imgdescRight" src="<?php echo $sub_resource['res_retailpro']['url']; ?>" alt="<?php echo $sub_resource['res_retailpro']['url']; ?>">
					<p id="desc-resource-panel"><?php echo $sub_resource['desc_retailpro'] ?></p>
				</div>
			</div>
			<!--End  Description row1-->

			<div class="col-lg-12">
				<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 border_resources" id="oraclePanel">
					<img style="width: 100%; margin-top: 50%;" src="<?php echo $sub_resource['res_oracle']['url']; ?>" alt="<?php echo $sub_resource['res_oracle']['url']; ?>">
				</div>
				<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 border_resources" id="ivendPanel">
					<img style="width: 100%; margin-top: 30%;" src="<?php echo $sub_resource['res_ivend']['url']; ?>" alt="<?php echo $sub_resource['res_ivend']['url']; ?>">
				</div>
				<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 border_resources" id="venstarPanel">
					<img style="width: 100%; margin-top: 50%;" src="<?php echo $sub_resource['res_venstar']['url']; ?>" alt="<?php echo $sub_resource['res_venstar']['url']; ?>">
				</div>
				<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 border_resources" id="accenturePanel">
					<img style="width: 100%; margin-top: 20%;" src="<?php echo $sub_resource['res_accenture']['url']; ?>" alt="<?php echo $sub_resource['res_accenture']['url']; ?>">
				</div>
			</div>
			<!-- Description row 2-->
			<div class="col-lg-12">
				<div class="col-xs-12 col-sm-3 col-md-3 col-lg-6 desc_panels" id="oracleDesc" >
					<img id="group_imgdescLeft" src="<?php echo $sub_resource['res_oracle']['url']; ?>" alt="<?php echo $sub_resource['res_oracle']['url']; ?>">
					<p id="desc-resource-panel"><?php echo $sub_resource['desc_oracle'] ?></p>
				</div>
				<div class="col-xs-12 col-sm-3 col-md-3 col-lg-offset-3 col-lg-6 desc_panels" id="ivendDesc">
					<img id="group_imgdescLeft" src="<?php echo $sub_resource['res_ivend']['url']; ?>" alt="<?php echo $sub_resource['res_ivend']['url']; ?>">
					<p id="desc-resource-panel"><?php echo $sub_resource['desc_ivend'] ?></p>
				</div>
				<div class="col-xs-12 col-sm-3 col-md-3 col-lg-offset-3 col-lg-6 desc_panels" id="venstarDesc">
					<img id="group_imgdescRight" src="<?php echo $sub_resource['res_venstar']['url']; ?>" alt="<?php echo $sub_resource['res_venstar']['url']; ?>">
					<p id="desc-resource-panel"><?php echo $sub_resource['desc_venstar'] ?></p>
				</div>
				<div class="col-xs-12 col-sm-3 col-md-3 col-lg-offset-6 col-lg-6 desc_panels" id="accentureDesc">
					<img id="group_imgdescRight" src="<?php echo $sub_resource['res_accenture']['url']; ?>" alt="<?php echo $sub_resource['res_accenture']['url']; ?>">
					<p id="desc-resource-panel"><?php echo $sub_resource['desc_accenture'] ?></p>
				</div>
			</div>
			<!--End  Description row 2-->
			<div class="col-xs-12 col-sm-12 col-md-offset-1 col-md-10 col-lg-7 col-lg-offset-2">
				<p id="text-resource-partner"><?php echo $sub_resource['subtitle_respage'] ?></p>
			</div>	
		<?php endif; ?>
</div>
</div>