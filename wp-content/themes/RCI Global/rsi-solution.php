<div class="blackground">
	<div class="rcigs-page page3 slideInUp hide" id="rsi_consulting_js">
		<div class="home img_rsibackconsulting">
		<div class="image-on-rsi hidden">
            <img id="image-ppl-meeting"src="wp-content/themes/RCI%20Global/assets/Images/u115.jpg" alt="">
        </div>
		<!-- rci description -->
			<div class="row main-description" style="margin:0;">
				<div class="col-xs-12">
                    <img id="background_rsi_solution" src="http://localhost:8089/RCI%20Global/wp-content/uploads/2019/01/u115-1.jpg" alt="">
                </div>
				<div class="col-sm-8 col-sm-offset-2 col-lg-6 col-md-6 col-md-offset-2 col-lg-offset-1 col-rci-description">
					<?php 
						$homesolution = get_field('rsi_solution');
						if($homesolution): ?>
						<p class="rci-consulting-description"><?php echo $homesolution['title_rcisolution'] ?></p>
						<a class="anchor-learn-more anchor-rsi-learn-more" href="https://rsiapac.com/">
							<button class="btn-learn-more rsi-learn-more-button">LEARN MORE</button>
						</a>
					<?php endif; ?>
				</div>
			</div>
		<!-- end of rci description -->
			<!-- the bottom row -->
			<div class="row row-rci-bottom">
				<!-- left rsi logo -->
				<div class="col-lg-4 col-sm-6 col-md-6 col-rsi-img-group">
					<img id="img-rsiconsulting" src="http://localhost:8089/RCI%20Global/wp-content/uploads/2019/01/Picture22.png" alt="">
					
				</div>
				<!-- end of left rsi logo -->
				<!-- right image group -->
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-8 col-image-with-title-rsi">
					<?php 
						$rsi_detail = get_field('rsi_solution');
						if($rsi_detail): ?>
							<div class="row row-ecommerce" >
								<div class="col-lg-3 text-left img_point">
										<div class=" rsi-image-border">
											<img id="img-rci-commerce" src="<?php echo $rsi_detail['img_rci-ecommerce']['url']; ?>" alt="<?php echo $rsi_detail['img_rci-ecommerce']['url']; ?>">
										</div>
										<p class="img-title"><?php echo $rsi_detail['ecommerce'] ?></p>
								</div>
								<div class="col-lg-4 text-left">
									<div class=" rsi-image-border">
									<img id="img-ordermanagement" src="<?php echo $rsi_detail['img_ordermanagement']['url']; ?>" alt="<?php echo $rsi_detail['img_ordermanagement']['url']; ?>">
									</div>	
									<p class="img-title"><?php echo $rsi_detail['title_ordermanagement'] ?></p>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-3 text-left img_point">
									<div class=" rsi-image-border">
									<img id="img-pointofsale" src="<?php echo $rsi_detail['img_pointofsale']['url']; ?>" alt="<?php echo $rsi_detail['img_pointofsale']['url']; ?>">
									</div>
									<p class="img-title"><?php echo $rsi_detail['title_pointofsale'] ?></p>
								</div>
								<div class="col-lg-4 text-left">
									<div class=" rsi-image-border">
									<img id="img-rsi-detail" src="<?php echo $rsi_detail['img_tailored']['url']; ?>" alt="<?php echo $rsi_detail['img_tailored']['url']; ?>">
									</div>	
									<p class="img-title"><?php echo $rsi_detail['title_tailored'] ?></p>
								</div>
							</div>
					<?php endif; ?>
				</div>
					<!-- end of right image group -->
			</div>
			<!-- end of bottom row -->
		</div>
	</div>
</div>