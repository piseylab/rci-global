<!-- Template Name: RCI Page -->
<script type="text/javascript">
	function show_menu_panel(){
		var menu = document.getElementById('menu-background');
			menu.style.display = 'block';
		var item = document.getElementById('menu-item-display');
			item.style.display = 'block';
	}
	function hide_menu_panel(){
		var menu = document.getElementById('menu-background');
			menu.style.display = 'none';
	   var item = document.getElementById('menu-item-display');
	   		item.style.display = 'none';
	}
	function show_rci_hover_header(){
		var rci_hover_header = document.getElementById('rci_hover_header').style;
		rci_hover_header.display = 'block';
		rci_hover_header.cursor = 'pointer';
	}
	function hide_rci_hover_header(){
		var rci_hover_header = document.getElementById('rci_hover_header').style;
		rci_hover_header.display = 'none';
	}
	function show_rsi_hover_header(){
		var rsi_hover_header = document.getElementById('rsi_hover_header').style;
		rsi_hover_header.display = 'block';
		rsi_hover_header.marginLeft = '50%';
		rsi_hover_header.cursor = 'pointer';
	}
	function hide_rsi_hover_header(){
		var rsi_hover_header = document.getElementById('rsi_hover_header').style;
		rsi_hover_header.display = 'none';
	}
</script>
<div class="rcigs-page ">
	<div class="home img_backheader">
		<div class="row row-head">
			<div class="col-lg-6 text-left">
				<?php 
				$image = get_field('rci_global_logo');
				if( !empty($image) ): ?>
					<div class="div_rci_logo">
						<img class="rci_global_logo " src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
						<p class="text-center logo_title"><?php the_field('title_logo'); ?></p> 
					</div>
				<?php endif; ?>
			</div>
			<div class="col-lg-6 col-newscenter text-right">
				<div class="newcenter-wrap pull-right">
					<?php 
					$image_newcenter = get_field('new_center_logo');
					if( !empty($image_newcenter) ): ?>
						<span class="title_newscenter pull-left">NEWS<br>CENTER</span>
						<div class="image_newcenter_block pull-left">
							<img class="image_newcenter"  src="http://localhost:8089/RCI%20Global/wp-content/uploads/2018/12/u934.png" alt="<?php echo $image_newcenter['alt']; ?>" />
						</div>
					<?php endif; ?>
				</div>
			</div>
			</div>
			<!-- End of row-head -->
		<div class="row row-rci-global" style="margin:0;">
			<div class="col-lg-7 col-rci-globalservice">
				<?php 
					$rci_global_services = get_field('rci_global_services');
					if($rci_global_services): ?>
				<h1 class="rci-global-heading"><?php echo $rci_global_services['title_rcigolbalservices']?></h1>
				<p class="rci-global-description"><?php echo $rci_global_services['sub_rciglobalservices'] ?></p>
				<a class="anchor-learn-more" href="<?php echo $rci_global_services['btn_rciglobalservices'] ?>">
					<button class="btn-learn-more ">LEARN MORE</button>
				</a>
				<?php endif; ?>
			</div>
			<div class="col-lg-5 col-search-menu">
				<div class="row" style="margin: 0;">  
					<div class="col-lg-4 col-lg-offset-8"> 
						<div class="seach-box"> 
							<input  id="search-box" type="text"/>
						</div>
						<div class="menu" 
							onmouseover="setTimeout(show_menu_panel, 2000)"
							onclick="show_menu_panel()">
							<div class="menu-icon-title">
								<div class="menu-icon">
									<img class="img-menu-icon" src="wp-content/themes/RCI%20Global/assets/Images/home/u922.png" alt="">		
									<img class="img-menu-icon" src="wp-content/themes/RCI%20Global/assets/Images/home/u922.png" alt="">
									<img class="img-menu-icon" src="wp-content/themes/RCI%20Global/assets/Images/home/u922.png" alt="">
								</div>
								<span id="title-menu"> Menu </span>
							</div>	
							<div id="menu-background-item-display"> 
								<div id="menu-background" onmouseout="setTimeOut(hide_menu_panel, 3000)">
									<div id="menu-item-display" >
										<?php get_header(); ?>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- header -->
	<div class="home_consul_solution">
		<div class="row" style="margin: 0;">
			<div class="col-lg-6 text-center col-rci" onmouseover="show_rci_hover_header()" >
				<div class="rci-background">
					<?php 
						$consul = get_field('retail_consultion');
						if($consul): ?>
							<p class="title-rci"><?php echo $consul['title_retailconsulting'] ?></p>
							<h1 class="rci-moto"><?php echo $consul['sub_retailconsulting'] ?></h1>
					<?php endif; ?>
				</div>
			</div>
			<div class="col-lg-6 text-center col-rsi" onmouseover="show_rsi_hover_header()">
				<div class="rsi-background">
					<?php 
						$solution = get_field('retail_solution');
						if($solution): ?>
							<p class="title-rsi"><?php echo $solution['title_retailsolution'] ?></p>
							<h1 class="rsi-moto"><?php echo $solution['sub_retailsolution'] ?></h1>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
	<!-- header On Hover -->
	<div class="home_consul_solution_hover">
		<div class="row" style="margin: 0;">
			<div class="col-lg-6 text-center col-rci-hover" id="rci_hover_header" onmouseout="hide_rci_hover_header()">
				<div class="rci-background-hover">
					<img id="image-rci-hover" src="wp-content/themes/RCI%20Global/assets/Images/home/u70.png" alt="">
				</div>
			</div>
			<div class="col-lg-6 text-center col-rsi-hover"  id="rsi_hover_header" onmouseout="hide_rsi_hover_header()">
				<div class="rsi-background-hover">
					<img id="image-rci-hover" src="wp-content/themes/RCI%20Global/assets/Images/home/u75.png" alt="">
				</div>
			</div>
		</div>
	</div>
</div>
<!-- =========News Center========= -->
<div class="newscenter">
	<div class="row" style="margin: 0;">
		<div class="col-lg-3">
			<div class="map-background">
				<h3 class="service-title inductry">RCI GLOBAL SERVICES <br>inductry news</h3>
			</div>
		</div>
		<div class="col-lg-3">
			<div class="pen-background">
				<h3 class="service-title case-study">RCI GLOBAL SERVICES<br>Case-study</h3>
			</div>
		</div>
		<div class="col-lg-3 ">
			<div class="light-background">
				<h3 class="service-title insight-article">RCI GLOBAL SERVICES<br>insight articles</h3>
			</div>
	    </div>
		<div class="col-lg-3 col-rgs-heading">
			<h1 class="newscenter-heading"><span id="title-r">RCI</span>
			<br><span id="gs">GLOBAL<br>SERVICES</span><br>
			<span id="title-nc">News<br>Center</span></h1>
		</div>
	</div>
</div>
<!-- SUB PAGE RCI GLOBAL	 -->
<div class="rcigs-about"> 
		<div class="container">
			<div class="row">
				<div class="col-lg-12  text-center col-rsi-rci-about">
					<div class="rci-text-title text-center"> 
						<h1 class="rgs-box-title">About RCI Global Services</h1>
					</div>
					<?php 
						$about_rci = get_field('subpage_rciglobal');
						if($about_rci): ?>
							<div class="col-lg-10 col-lg-offset-1 text-center">
							<img id="image-rci-rsi" src="http://localhost:8089/RCI%20Global/wp-content/uploads/2018/12/r-cirsi.png" alt="">
							<p class="rci-rsi-description"><?php echo $about_rci['title_aboutrci'] ?></p>
							</div>
					<?php endif; ?>
				</div>
			</div>
		</div>
	<div class="row image-branch" style="margin: 0;"></div>
</div>
<!-- by the number	 -->
<div class="row" style="margin: 0;">
	<div class="by_number col-lg-12">
		<h3 class="title-by-the-numbers">BY THE NUMBERS</h3>
		<div class="row" style="margin: 0;">
		<?php 
			$number = get_field('subpage_rciglobal');
			if($number): ?>
				<div class="container text-center">
					<div class="col-lg-3">
						<h1 class="number"><?php echo $number['1st'] ?></h1>
						<h3 class="fields"><?php echo $number['oms_cartridge'] ?></h3>
						<p class="field-descriptions"><?php echo $number['in_japan'] ?></p>
					</div>
					<div class="col-lg-1 col-line">
						<img src="<?php echo $number['rci_liney']['url']; ?>" alt="<?php echo $number['rci_liney']['url']; ?>">
					</div>
					<div class="col-lg-4 ">
						<h1 class="number"><?php echo $number['6'] ?></h1>
						<h3 class="fields"><?php echo $number['offices'] ?></h3>
						<p class="field-descriptions"><?php echo $number['in_asia'] ?></p>
					</div>
					<div class="col-lg-1 col-line">
						<img src="<?php echo $number['rci_liney']['url']; ?>" alt="<?php echo $number['rci_liney']['url']; ?>">
					</div>
					<div class="col-lg-3">
						<h1 class="number"><?php echo $number['10+'] ?></h1>
						<h3 class="fields"><?php echo $number['partners'] ?></h3>
						<p class="field-descriptions"><?php echo $number['well_coorperated'] ?></p>
					</div>
				</div>

					<div class="container text-center">
					<div class="col-lg-3">
						<h1 class="number"><?php echo $number['20+'] ?></h1>
						<h3 class="fields"><?php echo $number['years'] ?></h3>
						<p class="field-descriptions"><?php echo $number['it_experience'] ?></p>
					</div>
					<div class="col-lg-1 col-line">
						<img src="<?php echo $number['rci_liney']['url']; ?>" alt="<?php echo $number['rci_liney']['url']; ?>">
					</div>
					<div class="col-lg-4">
						<h1 class="number"><?php echo $number['40+'] ?></h1>
						<h3 class="fields"><?php echo $number['customers'] ?></h3>
						<p class="field-descriptions"><?php echo $number['worldwide'] ?></p>
					</div>
					<div class="col-lg-1 col-line">
						<img src="<?php echo $number['rci_liney']['url']; ?>" alt="<?php echo $number['rci_liney']['url']; ?>">
					</div>
					<div class="col-lg-3">
						<h1 class="number"><?php echo $number['50+'] ?></h1>
						<h3 class="fields"><?php echo $number['projects'] ?></h3>
						<p class="field-descriptions"><?php echo $number['successfully'] ?></p>
					</div>
				</div>
			<?php endif; ?>
		</div>
	</div>
<div>
<!-- founder	 -->

<div class="container text-center">
	<div class="col-lg-8 col-lg-offset-2">
		<?php 
			$founder = get_field('subpage_rciglobal');
			if($founder): ?>
				<img class="rsi_linex" src="<?php echo $founder['rsi_linex']['url']; ?>" alt="<?php echo $founder['rsi_linex']['url']; ?>">
				<h1 class="titlle-founder"><?php echo $founder['title_founder'] ?></h1>
				<img class="founder-photo" src="<?php echo $founder['img_founder']['url']; ?>" alt="<?php echo $founder['img_founder']['url']; ?>">
				<h3 class="founder-name"><?php echo $founder['jean'] ?></h3>
				<p class="about-founder"><?php echo $founder['title_jean'] ?></p>
				<img class="rsi_linex" src="<?php echo $founder['rsi_linex']['url']; ?>" alt="<?php echo $founder['rsi_linex']['url']; ?>">
			<?php endif; ?>
	</div>
</div>

<!-- RCI CONSULTING -->
<div class="rcigs-page">
	<div class="home img_backconsulting">
		<div class="row row-head">
			<div class="col-lg-6 text-left">
				<?php 
				$image = get_field('rci_global_logo');
				if( !empty($image) ): ?>
					<div class="div_rci_logo">
						<img class="rci_global_logo " src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
						<p class="text-center logo_title"><?php the_field('title_logo'); ?></p> 
					</div>
				<?php endif; ?>
			</div>
			<div class="col-lg-6 col-newscenter text-right">
				<div class="newcenter-wrap pull-right">
					<?php 
					$image_newcenter = get_field('new_center_logo');
					if( !empty($image_newcenter) ): ?>
						<span class="title_newscenter pull-left">NEWS<br>CENTER</span>
						<div class="image_newcenter_block pull-left">
							<img class="image_newcenter"  src="http://localhost:8089/RCI%20Global/wp-content/uploads/2018/12/u934.png" alt="<?php echo $image_newcenter['alt']; ?>" />
						</div>
					<?php endif; ?>
				</div>
			</div>
		
			<!-- End of row-head -->
		<div class="row row-rci-global" style="margin:0;">
			<div class="col-lg-7 col-rci-globalservice">
				<?php 
					$homeconsulting = get_field('rci_consulting_path');
					if($homeconsulting): ?>
				<p class="rci-consulting-description"><?php echo $homeconsulting['path_title_rciconsulting'] ?></p>
				<a class="anchor-learn-more" href="<?php echo $homeconsulting['path_link_rciconsulting'] ?>">
					<button class="btn-learn-more rci-learn-more-button">LEARN MORE</button>
				</a>
				<?php endif; ?>
			</div>
			<div class="col-lg-5 col-search-menu">
				<div class="row" style="margin: 0;">  
					<div class="col-lg-4 col-lg-offset-8"> 
						<div class="seach-box"> 
							<input  id="search-box" type="text"/>
						</div>
						<div class="menu" 
							onmouseover="setTimeout(show_menu_panel, 2000)"
							onclick="show_menu_panel()">
							<div class="menu-icon-title">
								<div class="menu-icon">
									<img class="img-menu-icon" src="wp-content/themes/RCI%20Global/assets/Images/home/u922.png" alt="">		
									<img class="img-menu-icon" src="wp-content/themes/RCI%20Global/assets/Images/home/u922.png" alt="">
									<img class="img-menu-icon" src="wp-content/themes/RCI%20Global/assets/Images/home/u922.png" alt="">
								</div>
								<span id="title-menu"> Menu </span>
							</div>	
							<div id="menu-background-item-display"> 
								<div id="menu-background" onmouseout="setTimeOut(hide_menu_panel, 3000)">
									<div id="menu-item-display" >
										<?php get_header(); ?>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row row-rci-bottom">
				<div class="col-lg-4 col-rci-img-group">
					<img id="img-rciconsulting" src="http://localhost:8089/RCI%20Global/wp-content/uploads/2019/01/rci-logo-with-text.png" alt="">
					<img id= "image-blue-triangle" src="http://localhost:8089/RCI%20Global/wp-content/uploads/2019/01/u85.png" alt="">
				</div>
					<div class="col-lg-8 col-image-with-title">
					<?php 
						$rci_detail = get_field('rci_consulting_path');
						if($rci_detail): ?>
							<div class="row row-technology" >
								<div class="col-lg-4 col-lg-offset-2 text-left">
										<div class="image-border">
											<img id="img-technology" src="<?php echo $rci_detail['img_technology']['url']; ?>" alt="<?php echo $rci_detail['img_technology']['url']; ?>">
										</div>
										<p class="img-title technology-title"><?php echo $rci_detail['technology'] ?></p>
								</div>
								<div class="col-lg-6 text-left">
									<div class="image-border">
										<img id="img-omnichannel" src="<?php echo $rci_detail['img_omnichannel']['url']; ?>" alt="<?php echo $rci_detail['img_omnichannel']['url']; ?>">
									</div>	
									<p class="img-title omnichannel-title"><?php echo $rci_detail['title_omnichannel'] ?></p>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-4 col-lg-offset-2 text-left">
									<div class="image-border">
										<img id="img-etailing" src="<?php echo $rci_detail['img_etailing']['url']; ?>" alt="<?php echo $rci_detail['img_etailing']['url']; ?>">
									</div>
									<p class="img-title e-tail-titile"><?php echo $rci_detail['title_etailing'] ?></p>
								</div>
								<div class="col-lg-6 text-left">
									<div class="image-border">
										<img id="img-advance" src="<?php echo $rci_detail['img_advance']['url']; ?>" alt="<?php echo $rci_detail['img_advance']['url']; ?>">
									</div>	
									<p class="img-title advnace-title"><?php echo $rci_detail['title_avance'] ?></p>
								</div>
							</div>
					<?php endif; ?>
					</div>
				</div>
			</div>
	  </div>
</div>

<!-- RSI CONSULTING -->
<div class="rcigs-page">
	<div class="home img_backconsulting">
		
		<div class="row row-rci-global" style="margin:0;">
			<div class="col-lg-7 col-rci-globalservice">
			<?php 
				$homesolution = get_field('rsi_solution');
				if($homesolution): ?>
				<p class="rci-consulting-description"><?php echo $homesolution['title_rcisolution'] ?></p>
				<a class="anchor-learn-more" href="<?php echo $homesolution['link_rcisolution'] ?>">
					<button class="btn-learn-more rsi-learn-more-button">LEARN MORE</button>
				</a>
				<?php endif; ?>
			</div>
			
		</div>
		<div class="row row-rci-bottom">
				<div class="col-lg-4 col-rci-img-group">
					<img id="img-rciconsulting" src="http://localhost:8089/RCI%20Global/wp-content/uploads/2019/01/Picture11.png" alt="">
					<img id= "image-yellow-triangle" src="http://localhost:8089/RCI%20Global/wp-content/uploads/2019/01/u121.png" alt="">
				</div>
					<div class="col-lg-8 col-image-with-title">
					<?php 
						$rsi_detail = get_field('rsi_solution');
						if($rsi_detail): ?>
							<div class="row row-technology" >
								<div class="col-lg-4 col-lg-offset-2 text-left">
										<div class=" rsi-image-border">
											<img id="img-rci-commerce" src="<?php echo $rsi_detail['img_rci-ecommerce']['url']; ?>" alt="<?php echo $rsi_detail['img_rci-ecommerce']['url']; ?>">
										</div>
										<p class="img-title"><?php echo $rsi_detail['ecommerce'] ?></p>
								</div>
								<div class="col-lg-6 text-left">
									<div class=" rsi-image-border">
									<img id="img-ordermanagement" src="<?php echo $rsi_detail['img_ordermanagement']['url']; ?>" alt="<?php echo $rsi_detail['img_ordermanagement']['url']; ?>">
									</div>	
									<p class="img-title"><?php echo $rsi_detail['title_ordermanagement'] ?></p>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-4 col-lg-offset-2 text-left">
									<div class=" rsi-image-border">
									<img id="img-pointofsale" src="<?php echo $rsi_detail['img_pointofsale']['url']; ?>" alt="<?php echo $rsi_detail['img_pointofsale']['url']; ?>">
									</div>
									<p class="img-title"><?php echo $rsi_detail['title_pointofsale'] ?></p>
								</div>
								<div class="col-lg-6 text-left">
									<div class=" rsi-image-border">
									<img id="img-rsi-detail" src="<?php echo $rsi_detail['img_tailored']['url']; ?>" alt="<?php echo $rsi_detail['img_tailored']['url']; ?>">
									</div>	
									<p class="img-title"><?php echo $rsi_detail['title_tailored'] ?></p>
								</div>
							</div>
					<?php endif; ?>
					</div>
				</div>
			</div>
	   </div>
<!-- CUSTOMER -->
<div class="rcigs-page">
	<div class="home img-customerbackground">
		<div class="row row-head">
			<div class="col-lg-6 text-left">
				<?php 
				$image = get_field('rci_global_logo');
				if( !empty($image) ): ?>
					<div class="div_rci_logo">
						<img class="rci_global_logo " src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
						<p class="text-center logo_title"><?php the_field('title_logo'); ?></p> 
					</div>
				<?php endif; ?>
			</div>
			<div class="col-lg-6 col-newscenter text-right">
				<div class="newcenter-wrap pull-right">
					<?php 
					$image_newcenter = get_field('new_center_logo');
					if( !empty($image_newcenter) ): ?>
						<span class="title_newscenter pull-left">NEWS<br>CENTER</span>
						<div class="image_newcenter_block pull-left">
							<img class="image_newcenter"  src="http://localhost:8089/RCI%20Global/wp-content/uploads/2018/12/u934.png" alt="<?php echo $image_newcenter['alt']; ?>" />
						</div>
					<?php endif; ?>
				</div>
			</div>
		</div>
		<!-- End of row-head -->
		<div class="row row-rci-global" style="margin:0;">
			<div class="col-lg-7 col-rci-globalservice">
				<?php 
				$homecustomer = get_field('home_customer');
				if($homesolution): ?>
				<h1 class="title-customer"><?php echo $homecustomer['title_homecustomer'] ?></h1>
				<p class="rci-consulting-description"><?php echo $homecustomer['sub_homecustomer'] ?></p>
				<a class="anchor-learn-more" href="<?php echo $homecustomer['link_homecustomer'] ?>">
					<button class="btn-learn-more customer-learn-more-button">LEARN MORE</button>
				</a>
				<?php endif; ?>
			</div>
			<div class="col-lg-5 col-search-menu">
				<div class="row" style="margin: 0;">  
					<div class="col-lg-4 col-lg-offset-8"> 
						<div class="seach-box"> 
							<input  id="search-box" type="text"/>
						</div>
						<div class="menu" 
							onmouseover="setTimeout(show_menu_panel, 2000)"
							onclick="show_menu_panel()">
							<div class="menu-icon-title">
								<div class="menu-icon">
									<img class="img-menu-icon" src="wp-content/themes/RCI%20Global/assets/Images/home/u922.png" alt="">		
									<img class="img-menu-icon" src="wp-content/themes/RCI%20Global/assets/Images/home/u922.png" alt="">
									<img class="img-menu-icon" src="wp-content/themes/RCI%20Global/assets/Images/home/u922.png" alt="">
								</div>
								<span id="title-menu"> Menu </span>
							</div>	
							<div id="menu-background-item-display"> 
								<div id="menu-background" onmouseout="setTimeOut(hide_menu_panel, 3000)">
									<div id="menu-item-display" >
										<?php get_header(); ?>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- End of Row-global -->
		<!-- Row-bottom-image -->
		<div class="row" style="margin: 0 auto;">
			<div class="col-lg-6 col-lg-offset-2">
				<?php 
					$img_homecustomer = get_field('home_customer');
					if($img_homecustomer): ?>
						<div class="row">
							<div class="col-lg-1">
								<img  id="img-homepandora" src="<?php echo $img_homecustomer['img_homepandora']['url']; ?>" 
								alt="<?php echo $img_homecustomer['img_homepandora']['url']; ?>">
							</div>
							<div class="col-lg-1">
								<img  id="img-homelv" src="<?php echo $img_homecustomer['img_homelv']['url']; ?>" 
								alt="<?php echo $img_homecustomer['img_homelv']['url']; ?>">
							</div>
							<div class="col-lg-1">
								<img id="img-homegucci" src="<?php echo $img_homecustomer['img_homegucci']['url']; ?>" 
								alt="<?php echo $img_homecustomer['img_homegucci']['url']; ?>">
							</div>
							<div class="col-lg-1">
								<img id="img-homeloreal"src="<?php echo $img_homecustomer['img_homeloreal']['url']; ?>" 
								alt="<?php echo $img_homecustomer['img_homeloreal']['url']; ?>">
							</div>
						</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
</div>

