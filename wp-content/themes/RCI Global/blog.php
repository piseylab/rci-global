<?php
   /* 
      Template Name: Blog-Page 
   */
   ?>
<?php 
   if(have_posts()):                     
   while(have_posts()): the_post(); 
   ?>
<?php get_header();?>
<header class="container-fluid head-blog">
   <div class="row">
      <div class="col-xs-2 col-md-2 col-lg-2 col-lg-offset-1 pull-left">
         <div class="logo">
            <img src="http://localhost:8089/RCI%20Global/wp-content/uploads/2019/01/u16.png" alt="" style="height:70px;">	
         </div>
      </div>
      <div class="col-xs-2 col-sm-8 col-md-8 col-lg-8">
         <nav class="navbar navbar-default">
            <div class="container">
               <div class="navbar-header">
                  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                  <span class="sr-only">Toggle navigation</span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  </button>
                  <a class="navbar-brand" href="#">Brand</a>
               </div>
                  <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                     <ul class="nav navbar-nav">
                        <li class="active"><a href="#">RCI Consulting <span class="sr-only">(current)</span></a></li>
                        <li><a href="#">RSI Solutions</a></li>
                        <li><a href="#">Customers</a></li>
                        <li><a href="#">Resources</a></li>
                        <li><a href="#">Blog</a></li>
                     </ul>
                     </li>
                     </ul>
                  </div>
                  <!-- /.navbar-collapse -->
               </div>
               <!-- /.container-fluid -->
            </nav>
         </div>
      </div>
   </div>
</header>
   <section>
      <div class="row">
         <div class="col-xs-12 col-sm-6 col-md-7 col-lg-7">
            <p>
               <a href="#">Blog</a>
            </p>
         </div>
      </div>
      <div class="row">
         <div class="col-xs-12 col-sm-6 col-md-7 col-lg-7">
            <div>
               <img class="back-img-blog" src="http://localhost:8089/RCI%20Global/wp-content/uploads/2019/01/u115-1.jpg" alt="" style="height: 650px;">
               <article class="post">
                  <div <?php post_class();?> id="post-<?php the_ID(); ?>">
                     <h3><?php the_date( 'Y.m.d' );?></h3>
                     <p class="post-meta">
                        <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                     </p>
                  </div>
               </article>
            </div>
         </div>
         <div class="col-xs-12 col-sm-6 col-md-5 col-lg-5">
            <div class="row">
               <div class="col-xs-12">
                  <div>
                     <img class="back-img-blog" src="http://localhost:8089/RCI%20Global/wp-content/uploads/2019/01/u115-1.jpg" alt="" style="height: 300px;">
                     <article class="post">
                        <div <?php post_class();?> id="post-<?php the_ID(); ?>">
                           <h3><?php the_date( 'Y.m.d' );?></h3>
                           <p class="post-meta">
                              <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                           </p>
                        </div>
                     </article>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                  <div>
                     <img class="back-img-blog" src="http://localhost:8089/RCI%20Global/wp-content/uploads/2019/01/u115-1.jpg" alt="" style="height: 300px;">
                     <article class="post">
                        <div <?php post_class();?> id="post-<?php the_ID(); ?>">
                           <h3><?php the_date( 'Y.m.d' );?></h3>
                           <p class="post-meta">
                              <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                           </p>
                        </div>
                     </article>
                  </div>
               </div>
               <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                  <div>
                     <img class="back-img-blog" src="http://localhost:8089/RCI%20Global/wp-content/uploads/2019/01/u115-1.jpg" alt="" style="height: 300px;">
                     <article class="post">
                        <div <?php post_class();?> id="post-<?php the_ID(); ?>">
                           <h3><?php the_date( 'Y.m.d' );?></h3>
                           <p class="post-meta">
                              <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                           </p>
                        </div>
                     </article>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </section>
   <footer>

   </footer>

<?php endwhile;
   else:
   echo '<p>No Post found</p>'; 
   endif;
   ?>