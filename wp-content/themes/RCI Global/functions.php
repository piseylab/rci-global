<?php 
	
function rci_global(){
	wp_enqueue_style( 'style', get_stylesheet_uri() );
	wp_enqueue_style( 'style_rcigs', get_stylesheet_directory_uri().'/assets/css/rcigs.css');
	wp_enqueue_style( 'style_about', get_stylesheet_directory_uri().'/assets/css/about.css');
	wp_enqueue_style( 'style_consulting', get_stylesheet_directory_uri().'/assets/css/consulting.css');
	wp_enqueue_style( 'style_rsi', get_stylesheet_directory_uri().'/assets/css/rsi.css');
	wp_enqueue_style( 'style_customer', get_stylesheet_directory_uri().'/assets/css/customer.css');
	wp_enqueue_style( 'style_resources', get_stylesheet_directory_uri().'/assets/css/resources.css');
	wp_enqueue_style( 'style_contact', get_stylesheet_directory_uri().'/assets/css/contact.css');
	wp_enqueue_style( 'style_blog', get_stylesheet_directory_uri().'/assets/css/blog.css');
}
add_action( 'wp_enqueue_scripts', 'rci_global' );

	// nav bar
	register_nav_menus( 
		array(
				'primary' => __('Primary Menu'),
				'footer' => __('Footer Menu')
		)
	);
 ?>