	<footer class="site-footer">
		<div class="footer-content">
			<div class="row row-container" style="margin-right: 0; margin-left: 0;">
				<div class="col-xs-12 col-sm-offset-0 col-sm-12 col-md-offset-1 col-md-6 col-lg-4 col-lg-offset-1 col-dis text-center">
					<h1 class="footer-title"><?php the_field('rci_global'); ?></h1>
				</div>
				<div class="col-xs-12 col-lg-6 col-right-title col-dis text-center"> 
					<?php 
					$footer_title = get_field('footer_title_menu');
					if($footer_title):  ?>
						<div class="row" style="margin: 0;">
						 	<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 col-right-items text-right" style="padding-right: 0;">
								<ul>
									<li><p class="right-title"><?php echo $footer_title['rci_title'] ?></p></li>
									<li><p class="right-title"><?php echo $footer_title['rsi_title'] ?></p></li>
									<li><p class="right-title"><?php echo $footer_title['customer_title'] ?></p></li>							
									<li><p class="right-title"><?php echo $footer_title['resources'] ?></p></li>
								</ul>
							</div>
						</div>
						
					<?php endif; ?>
				</div>
			</div>
			<div class="row" style="margin-right: 0; margin-left: 0;">
				<div class="col-lg-10 col-lg-offset-1">
					<?php 
						$line_footer = get_field('line-footer');
						if(!empty($line_footer)): ?>
<!-- 							<img style="height: 4px; width: 100%;" src="<?php echo $line_footer['url']; ?>" alt="<?php echo $line_footer['alt']; ?>" /> -->
					<div class="line_footer">
						
					</div>
						<?php endif; ?>
				</div>
			</div>
			<div class="row row-contact-footer" style="margin-left: 0; margin-right: 0;">
				<div class="col-lg-10 col-lg-offset-1">
					<?php $footer = get_field('footer');
					if($footer) : ?>
					<div class="col-xs-12 col-sm-4 col-md-4 col-lg-3" style="padding-left: 0;">
						<p class="title-footer-contact"><?php echo $footer['title_footer_con']; ?></p>
						<p class="sub-footer-contact"><?php echo $footer['sub_footer_con']; ?></p> 
					</div>
					<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 col-address" style="padding-left: 0px;">
						<p><?php echo $footer['address']; ?></p> 
					</div>
					<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 col-lg-offset-1 text-right col-rsi-rci" style="padding-right: 0;">
						<br>
						<img class="img_rci_footer" src="<?php echo $footer['img_rci']['url']; ?>" alt="<?php echo $footer['img_rci']['alt']; ?>" /> &nbsp;&nbsp;
						<img class="img_verticleline_footer" src="<?php echo $footer['line_footer_img']['url']; ?>" alt="<?php echo $footer['line_footer_img']['alt']; ?>" />&nbsp;&nbsp;
						<img class="img_rsi_footer" src="<?php echo $footer['img_rsi']['url']; ?>" alt="<?php echo $footer['img_rsi']['alt']; ?>"  />
					</div>
					<?php endif; ?>
				</div>
			</div>
		</div>
		<div class="wrap-social">
			<div class="container">
				<div class="row text-center">
					<i class="fab fa-facebook-square" style="font-size: 40px; margin-right: 20px; "></i>
					<i class="fab fa-linkedin-in" style="font-size: 40px;"></i>
					<p style="margin-top: 6px; font-size: 14px; font-family: 'Arial' ; "><?php the_field('copy_right'); ?></p>
				</div>
			</div>
			<div class="container text-center">
				<p><?php echo date('Y'); ?> Retail Consulting Innovation</p>
			</div>
		</div>
	</footer>
<!-- end of container	 div-->
	<?php wp_footer(); ?>
	</body>
</html>